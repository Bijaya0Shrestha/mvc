﻿using Microsoft.AspNetCore.Mvc;
using OnlineProject.Models;
using OnlineProject.Data;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using OnlineProject.Migrations;
using OnlineProject.Data;

namespace OnlineProject.Controllers
{
    public class SignupController : Controller 
    { 
        private readonly OnlineProjectContext _context;
        public SignupController(OnlineProjectContext context)
        {
            _context = context;
        }
              
             public IActionResult Index()
             {
            var data = _context.Signups.ToList();
              return View(data);
             }

            [HttpGet]
            public IActionResult Form( )
            {
              return View();
            }
            [HttpPost]
            public IActionResult Form(Signup s)
            {
                _context.Signups.Add(s);
                 _context.SaveChanges();
                return View();
            }

         
    }
}
