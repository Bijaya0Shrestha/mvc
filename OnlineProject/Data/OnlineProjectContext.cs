﻿using Microsoft.EntityFrameworkCore;

using OnlineProject.Models;

namespace OnlineProject.Data
{
    public class OnlineProjectContext :DbContext
    {
        public OnlineProjectContext(DbContextOptions<OnlineProjectContext>  options) : base(options)
        { 
        
        }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Products> Products { get; set; }
        public DbSet<Signup> Signups { get; set; }
    }
}
