﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineProject.Models
{
    public class Products
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Brand { get; set; }

        [ForeignKey("Customers")]
        public int CustomerId { get; set; }
        public virtual Customers Customers { get; set; }
    }
}
