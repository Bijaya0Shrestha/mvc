﻿using System.ComponentModel.DataAnnotations;

namespace OnlineProject.Models
{
    public class Signup
    {
        [Key]
        
        public int sId { get; set; }
        [Required(ErrorMessage="Please Enter a valid Name")]
        [StringLength(20),MinLength(5)]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please Enter a valid Address")]
        [StringLength(30,MinimumLength=5)]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter a Phone number")]
        [RegularExpression(@"^98[0-9]{8}$")]
        [StringLength(10, ErrorMessage = "Phone number must be 10 digit"),MinLength(10)]

        public string Phone { get; set; }
        [Required(ErrorMessage ="Please Enter a valid Email")]
        [EmailAddress]
        public string Email { get; set; }
        [Required (ErrorMessage ="Please Enter a valid Password")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$")]
        public string Password { get; set; }
        [Required (ErrorMessage ="Your password doesnot match")]
        [Compare("Password")]
        public string CPassword { get; set; }
        [Required(ErrorMessage = "You are missing this field")]
        public Gender Gender { get; set; }
            
    }

    public enum Gender
    {
        Male, Female, Others
    }
}
